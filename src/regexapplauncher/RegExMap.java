package regexapplauncher;

/** Class RegExMap
 * Structure for mapping RegEx and apps
 * @author mlinton
 *
 */

public class RegExMap {
	public String regex = "";
	public String app = "";
	public String uri = "";
}
