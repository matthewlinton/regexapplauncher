package regexapplauncher;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.swt.layout.FillLayout;

public class RegExAppConfig extends Composite {
	private DataBindingContext m_bindingContext;
	private Table table_launchers;
	private Button btn_close;
	private Text txt_copyrightnotice;
	private Text txtProgramAndVersion;
	
	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public RegExAppConfig(Composite parent, int style, String str_programandversion) {
		super(parent, style);
		setLayout(null);
		
		Composite composite = new Composite(this, SWT.NONE);
		composite.setBounds(0, 0, 379, 284);
		composite.setLayout(null);
		
		TabFolder tabFolder = new TabFolder(composite, SWT.NONE);
		tabFolder.setSize(378, 246);
		
		TabItem tbtm_launchers = new TabItem(tabFolder, SWT.NONE);
		tbtm_launchers.setText("Launchers");
		
		Composite composite_launchers = new Composite(tabFolder, SWT.NONE);
		tbtm_launchers.setControl(composite_launchers);
		
		table_launchers = new Table(composite_launchers, SWT.BORDER | SWT.FULL_SELECTION);
		table_launchers.setHeaderBackground(SWTResourceManager.getColor(227, 227, 227));
		table_launchers.setBackground(SWTResourceManager.getColor(255, 255, 255));
		table_launchers.setHeaderVisible(true);
		table_launchers.setLinesVisible(true);
		table_launchers.setBounds(10, 10, 350, 198);
		
		TableColumn tblclmn_launchers_name = new TableColumn(table_launchers, SWT.NULL);
		tblclmn_launchers_name.setWidth(78);
		tblclmn_launchers_name.setText("Name");
		
		TableColumn tblclmn_launchers_regex = new TableColumn(table_launchers, SWT.NULL);
		tblclmn_launchers_regex.setWidth(105);
		tblclmn_launchers_regex.setText("Regex");
		
		TableColumn tblclmn_launchers_execute = new TableColumn(table_launchers, SWT.LEFT);
		tblclmn_launchers_execute.setWidth(265);
		tblclmn_launchers_execute.setText("Program");
		
		TableItem tableItem = new TableItem(table_launchers, SWT.NONE);
		tableItem.setText(new String[] {"0", "*.google.com/*", "C:\\Program Files\\Google\\Chrome\\Chrome.exe"});
		tableItem.setText("Google");
		
		TabItem tbtm_settings = new TabItem(tabFolder, SWT.NONE);
		tbtm_settings.setText("Settings");
		
		Composite composite_settings = new Composite(tabFolder, SWT.NONE);
		tbtm_settings.setControl(composite_settings);
		
		TabItem tbtm_about = new TabItem(tabFolder, SWT.NONE);
		tbtm_about.setText("About");
		
		Composite composite_about = new Composite(tabFolder, SWT.NONE);
		tbtm_about.setControl(composite_about);
		composite_about.setLayout(null);
		
		Label lbl_devs = new Label(composite_about, SWT.NONE);
		lbl_devs.setBounds(20, 31, 85, 15);
		lbl_devs.setText("Developers:");
		
		txt_copyrightnotice = new Text(composite_about, SWT.BORDER | SWT.READ_ONLY | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
		txt_copyrightnotice.setText("Permission is hereby granted, free of charge, to any person obtaining a copy\r\nof this software and associated documentation files (the \"Software\"), to deal\r\nin the Software without restriction, including without limitation the rights\r\nto use, copy, modify, merge, publish, distribute, sublicense, and/or sell\r\ncopies of the Software, and to permit persons to whom the Software is\r\nfurnished to do so, subject to the following conditions:\r\n\r\nThe above copyright notice and this permission notice shall be included in all\r\ncopies or substantial portions of the Software.\r\n\r\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\r\nIMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\r\nFITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\r\nAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\r\nLIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\r\nOUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\r\nSOFTWARE.\r\n\r\nSee the included \"LICENSE.MIT\" for more information");
		txt_copyrightnotice.setBounds(10, 121, 350, 87);
		
		Label lblNewLabel = new Label(composite_about, SWT.NONE);
		lblNewLabel.setBounds(10, 100, 350, 15);
		lblNewLabel.setText("Copyright (c) 2018 Matthew Linton");
		
		txtProgramAndVersion = new Text(composite_about, SWT.READ_ONLY);
		txtProgramAndVersion.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		txtProgramAndVersion.setText(str_programandversion);
		txtProgramAndVersion.setEditable(true);
		txtProgramAndVersion.setBounds(10, 10, 350, 15);
		
		Label lblMatthewLinton = new Label(composite_about, SWT.NONE);
		lblMatthewLinton.setBounds(111, 31, 128, 15);
		lblMatthewLinton.setText("Matthew Linton");
		
		btn_close = new Button(composite, SWT.NONE);
		btn_close.setLocation(293, 250);
		btn_close.setSize(75, 25);
		btn_close.setText("Close");
		btn_close.addSelectionListener(new SelectionAdapter() {
		    @Override
		        public void widgetSelected(SelectionEvent e) {
		    		parent.dispose();
		     }
		});
		
		Button btn_save = new Button(composite, SWT.NONE);
		btn_save.setBounds(212, 250, 75, 25);
		btn_save.setText("Save");

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
