package regexapplauncher;

/** Class RegExApp
 * Will parse the target URI using expressions added to the mapping array
 * @author mlinton
 *
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExApp {
	public static int max_map = 1024;
	
	private RegExMap[] arr_map = new RegExMap[max_map];
	private int idx_map = 0;
	private int len_map = 0;
	
	RegExApp() { }
	RegExApp(RegExMap match) { }
	RegExApp(RegExMap match[]) { }
	
	// Load a regex statement into the mapping array
	public RegExApp add_RegExMap (RegExMap rem_match) {
		if (idx_map < max_map) {
			arr_map[idx_map] = new RegExMap();
			arr_map[idx_map].app = rem_match.app;
			arr_map[idx_map].regex = rem_match.regex;
			arr_map[idx_map].uri = rem_match.uri;
			len_map++;
			idx_map++;
		}
		return this;
	}
	
	// clear the mapping array
	public RegExApp clear_RegExMap() {
		idx_map = 0;
		len_map = 0;
		return this;
	}
	
	// Get the command that will be run to launch the URI
	public RegExMap get_RunCommand() {
		Pattern ptn_regex;
		Matcher mtc_uri;
		
		// loop through the mapping and look for matches
		for (idx_map = 0; idx_map <= len_map; idx_map++) {
			if (arr_map[idx_map] != null ) {
				//System.out.println("Checking - " + idx_map + " : " + arr_map[idx_map].regex + " : " + arr_map[idx_map].uri);
				ptn_regex = Pattern.compile(arr_map[idx_map].regex);
				mtc_uri = ptn_regex.matcher(arr_map[idx_map].uri);
				
				// Break on our first match
				if (mtc_uri.matches()) {
					//System.out.println("found a match at " + idx_map);
					break;
				}
				
			} else {
				// If we don't find a match create a blank entry to pass back
				arr_map[idx_map] = new RegExMap();
				arr_map[idx_map].app = "";
				arr_map[idx_map].regex = "";
				arr_map[idx_map].uri = "";
				break;
			}
		}
		return arr_map[idx_map];
	}
	
} // END RegExApp